<?php

use App\Chat;
use App\User;
use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('api/getchatsfor', [
        'as' => 'getChatListFor',
        function(Request $request){
            $myId = $request['myid'];
            $chId = $request['chid'];
            $chatRaw = Chat::where('sender', $myId)->orWhere('receiver', $myId);
            $chatRaw2 = $chatRaw->where('sender', $chId)->orWhere('receiver', $chId);
            $chats = $chatRaw2->orderBy('id', 'desc')->get();
            $toReturn = "";
            foreach ($chats as $chat){
                $appendTo = "";
                if($chat->sender == $myId){
                    $sender = User::where('id', $chat->sender)->first()->name;
                    $messageTime = \Carbon\Carbon::parse($chat->created_at)->diffForHumans();
                    $appendTo = "
                        <div class=\"chat-message\">
                            <div class=\"message\">
                                <a class=\"message-author\" href=\"#\">$sender</a>
                                <span class=\"message-date\"> $messageTime  </span>
                                <span class=\"message-content\">$chat->message</span>
                            </div>
                        </div>
                    ";
                }else{
                    $sender = User::where('id', $chat->sender)->first()->name;
                    $messageTime = \Carbon\Carbon::parse($chat->created_at)->diffForHumans();
                    $appendTo = "
                        <div class=\"chat-message\">
                            <div class=\"message\">
                                <span class=\"message-date\">$messageTime</span>
                                <a class=\"message-author\" href=\"#\">$sender</a>
                                <span class=\"message-content\">$chat->message</span>
                            </div>
                        </div>
                    ";
                }
                $toReturn = $toReturn."".$appendTo;
            }
            return $toReturn;
        }
    ]);

    Route::post('api/postchats', [
        'as' => 'postChat',
        function(Request $request){
            $chat = new Chat();
            $chat->sender = $request['myid'];
            $chat->receiver = $request['chid'];
            $chat->message = $request['message'];
            $chat->save();
            return "Jovi";
        }
    ]);

