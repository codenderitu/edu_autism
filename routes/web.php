<?php


use App\Chat;
use App\Http\Controllers\FuncController;
use App\Skill;
use App\Update;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;

Route::get('/', [
        'as' => 'login',
        function () {
            return view('pages.auth.login');
        }
    ]);

    Route::get('logout', [
        'as' => 'logout',
        function(){
            $func = new FuncController();
            Auth::logout();
            return $func->toRouteWithMessage("login","Logged out successfully", "", "info");
        }
    ]);

    Route::post('postlogin', [
        'as' => 'postLogin',
        'uses' => 'FuncController@login'
    ]);

    Route::get('signup', [
        'as' => 'signup',
        function(){
            return view('pages.auth.signUp');
        }
    ]);

    Route::post('postSignUp', [
        'as' => 'postSignUp',
        'uses' => 'FuncController@signUp'
    ]);

    Route::get('forgotpassword', [
        'as' => 'forgotPassword',
        function(){
            return view('pages.auth.forgotPassword');
        }
    ]);

    Route::get('resetpassword', [
        'as' => 'resetPassword',
        function(){
            return view('pages.auth.resetPassword');
        }
    ]);

    Route::get('parent/home/{filter?}', [
        'as' => 'parentHome',
        function($filter = 'all'){
            if($filter == 'all'){
                $updates = Update::orderBy('id', 'desc')->paginate(20);
            }elseif ($filter == 'parent'){
                $updates = Update::where('postertype', 'parent')->orderBy('id', 'desc')->paginate(20);
            }elseif ($filter == 'caregiver'){
                $updates = Update::where('postertype', 'caregiver')->orderBy('id', 'desc')->paginate(20);
            }else{
                $updates = null;
            }

            return view('pages.parent.home', [
                'updates' => $updates
            ]);
        }
    ])->middleware('auth')->middleware('parent');

    Route::get('parent/discussions', [
        'as' => 'parentDiscussions',
        function(){
            $myId = Auth::user()->getAuthIdentifier();
            $chats = Chat::where('sender', $myId)
                ->orWhere('receiver', $myId)
                ->orderBy('id', 'desc')
                ->get();
            $chatUsers = array();
            foreach ($chats as $chat){
                if($chat->sender == $myId){
                    $otherUser = User::where('id', $chat->receiver)->first();
                }else{
                    $otherUser = User::where('id', $chat->sender)->first();
                }
                array_push($chatUsers, $otherUser);
            }
            $chatUsers = array_unique($chatUsers);
            return view('pages.parent.discussions', [
                'chatUsers' => $chatUsers
            ]);
        }
    ])->middleware('auth')->middleware('parent');

    Route::get('parent/profile', [
        'as' => 'parentProfile',
        function(){
            return view('pages.parent.profile');
        }
    ])->middleware('auth')->middleware('parent');

    Route::post('parent/startchat', [
        'as' => 'startChat',
        function(Request $request){
            $func = new FuncController();
            $chat = new Chat();
            $chat->sender = $request['myid'];
            $chat->receiver = $request['chid'];
            $chat->message = $request['message'];
            if($chat->save()){
                return $func->toRouteWithMessage("parentDiscussions", "Chat has been started", "", "success");
            }else{
                return $func->backWithUnknownError();
            }
        }
    ])->middleware('auth')->middleware('parent');

    Route::get('parent/caregivers', [
        'as' => 'parentCaregivers',
        function(){
            return view('pages.parent.careGivers');
        }
    ])->middleware('auth')->middleware('parent');

    Route::get('parent/updatepassword', [
        'as' => 'parentUpdatePassword',
        function(){
            return view('pages.parent.updatePassword');
        }
    ])->middleware('auth')->middleware('parent');

    Route::post('careGiverFilterHome', [
        'as' => 'careGiverFilterHome',
        function(Request $request){
            return redirect()->route('caregiverHome', ['filter' => $request['postertype']]);
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::get('caregiver/home/{filter?}', [
        'as' => 'caregiverHome',
        function($filter = 'all'){
            if($filter == 'all'){
                $updates = Update::orderBy('id', 'desc')->paginate(20);
            }elseif ($filter == 'parent'){
                $updates = Update::where('postertype', 'parent')->orderBy('id', 'desc')->paginate(20);
            }elseif ($filter == 'caregiver'){
                $updates = Update::where('postertype', 'caregiver')->orderBy('id', 'desc')->paginate(20);
            }else {
                $updates = null;
            }
            return view('pages.caregiver.home', [
                'updates' => $updates
            ]);
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postUpdateCaregiver', [
        'as' => 'postUpdateCaregiver',
        'uses' => 'FuncController@postUpdateCaregiver'
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postUpdateParent', [
        'as' => 'postUpdateParent',
        'uses' => 'FuncController@postUpdateCaregiver'
    ])->middleware('auth')->middleware('parent');

    Route::get('caregiver/discussions/{chatUser?}', [
        'as' => 'careGiverDiscussions',
        function($chatUser = 0){
            $myId = Auth::user()->getAuthIdentifier();
            $chats = Chat::where('sender', $myId)
                ->orWhere('receiver', $myId)
                ->orderBy('id', 'desc')
                ->get();
            $chatUsers = array();
            foreach ($chats as $chat){
                if($chat->sender == $myId){
                    $otherUser = User::where('id', $chat->receiver)->first();
                }else{
                    $otherUser = User::where('id', $chat->sender)->first();
                }
                array_push($chatUsers, $otherUser);
            }
            $chatUsers = array_unique($chatUsers);
            return view('pages.caregiver.discussions', [
                'chatUsers' => $chatUsers,
            ]);
        }
    ]);

    Route::get('caregiver/profile', [
        'as' => 'careGiverProfile',
        function(){
            return view('pages.caregiver.profile');
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postAddCaregiverSkill', [
        'as' => 'postAddCaregiverSkill',
        'uses' => 'FuncController@postAddCaregiverSkill'
    ])->middleware('auth')->middleware('caregiver');

    Route::get('caregiver/skills/delete/{skill}', [
        'as' => 'caregiverDeleteSkill',
        function($skillid){
            $func = new FuncController();
            $skillRaw = Skill::where('id', $skillid);
            if($skillRaw->count() == 1){
                $skill = $skillRaw->first();
                if($skill->user == Auth::user()->getAuthIdentifier()){
                    if($skill->delete()){
                        return $func->backWithMessage("Deleted", "","success");
                    }else{
                        return $func->backWithUnknownError();
                    }
                }else{
                    return $func->backWithMessage("Sorry", "This is not your skill","error");
                }
            }else{
                return $func->backWithMessage("Sorry", "Skill not found","error");
            }
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('careGiveProfileUpdate', [
        'as' => 'careGiveProfileUpdate',
        function(Request $request){
            $func = new FuncController();
            if($request->name != "" && $request != Auth::user()->name){
                $user = Auth::user();
                $user->name = $request['name'];
                $user->save();
            }
            if($request->hasFile('proffpic')){
                $photo = $request->file('proffpic');
                $ext = $ext = $photo->getClientOriginalExtension();
                if($ext!='jpg' && $ext!='JPG' && $ext != 'PNG' && $ext != 'png' && $ext != 'JPEG' && $ext != 'jpeg'
                    && $ext != 'bmp' && $ext != 'BMP' && $ext != 'gif' && $ext != 'GIF' && $ext != 'svg' && $ext != 'SVG'){
                    return $func->backWithMessage('Sorry', "That file format is not allowed", "error");
                }else{
                    $name = $func->generateRandomString(50).'.'.$ext;
                    if(Image::make($photo)->fit(590, 393, function ($contraint){})->save('storage/images/'.$name)){
                        $user = Auth::user();
                        $user->proffpic = $name;
                        $user->save();
                        return $func->justBack();
                    }
                }
            }
            return $func->justBack();
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postParentUpdateProfile', [
        'as' => 'postProfileUpdateProfile',
        function(Request $request){
            $func = new FuncController();
            $user = Auth::user();
            $user->aboutchild = $request['aboutchild'];
            $user->save();
            if($request->hasFile('proffpic')){
                $photo = $request->file('proffpic');
                $ext = $ext = $photo->getClientOriginalExtension();
                if($ext!='jpg' && $ext!='JPG' && $ext != 'PNG' && $ext != 'png' && $ext != 'JPEG' && $ext != 'jpeg'
                    && $ext != 'bmp' && $ext != 'BMP' && $ext != 'gif' && $ext != 'GIF' && $ext != 'svg' && $ext != 'SVG'){
                    return $func->backWithMessage('Sorry', "That file format is not allowed", "error");
                }else{
                    $name = $func->generateRandomString(50).'.'.$ext;
                    if(Image::make($photo)->fit(590, 393, function ($contraint){})->save('storage/images/'.$name)){
                        $user = Auth::user();
                        $user->proffpic = $name;
                        $user->save();
                        return $func->justBack();
                    }
                }
            }
            return $func->backWithMessage("Profile Updated", "", "success");
        }
    ])->middleware('auth')->middleware('parent');

    Route::get('caregiver/updatepassword', [
        'as' => 'caregiverUpdatePassword',
        function(){
            return view('pages.caregiver.updatePassword');
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postCareGiverUpdatePassword', [
        'as' => 'postCareGiverUpdatePassword',
        function(Request $request){
            $func = new FuncController();
            $user = Auth::user();
            if(! Auth::attempt(['username' => $user->username, 'password' => $request['currpass']])){
//                return bcrypt($request['currpass'])."<br>".$user->password;
                return $func->backWithMessage("Sorry", "Your current password is not correct", "error");
            }else{
                if($request['newpass'] == $request['conpass']){
                    $user->password = bcrypt('newpass');
                    if($user->save()){
                        return $func->backWithMessage("Password has been updted", "", "success");
                    }else{
                        return $func->backWithUnknownError();
                    }
                }else{
                    return $func->backWithMessage("The confirmation password does not equal the new password", "", "success");
                }
            }
        }
    ])->middleware('auth')->middleware('caregiver');

    Route::post('postParentUpdatePassword', [
        'as' => 'postParentUpdatePassword',
        function(Request $request){
            $func = new FuncController();
            $user = Auth::user();
            if(! Auth::attempt(['username' => $user->username, 'password' => $request['currpass']])){
//                return bcrypt($request['currpass'])."<br>".$user->password;
                return $func->backWithMessage("Sorry", "Your current password is not correct", "error");
            }else{
                if($request['newpass'] == $request['conpass']){
                    $user->password = bcrypt('newpass');
                    if($user->save()){
                        return $func->backWithMessage("Password has been updted", "", "success");
                    }else{
                        return $func->backWithUnknownError();
                    }
                }else{
                    return $func->backWithMessage("The confirmation password does not equal the new password", "", "success");
                }
            }
        }
    ])->middleware('auth')->middleware('parent');



