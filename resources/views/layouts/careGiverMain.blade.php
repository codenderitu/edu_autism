<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('theTitle')</title>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ \Illuminate\Support\Facades\URL::asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        @yield('theHeadCss')
    </head>
    <body>
        <div id="wrapper">
            @include('pages.caregiver.includes.sideNavBar')
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="row border-bottom">
                    @include('pages.caregiver.includes.topNavBar')
                </div>
                @yield('theBody')
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ \Illuminate\Support\Facades\URL::asset('js/plugins/toastr/toastr.min.js') }}"></script>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <script type="text/javascript">
                toastr.{{ \Illuminate\Support\Facades\Session::get('status') }}('{{ \Illuminate\Support\Facades\Session::get('message') }}', '{{ \Illuminate\Support\Facades\Session::get('title') }}');
            </script>
        @endif
        @yield('theJs')
    </body>
</html>