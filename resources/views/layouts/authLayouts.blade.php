<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title')</title>
        <link href="{{ \Illuminate\Support\Facades\URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ \Illuminate\Support\Facades\URL::asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ \Illuminate\Support\Facades\URL::asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
        <link href="{{ \Illuminate\Support\Facades\URL::asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ \Illuminate\Support\Facades\URL::asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
        @yield('theCSS')
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated pulse">
            <div>
                @yield('theContent')
            </div>
        </div>
        <script src="{{ \Illuminate\Support\Facades\URL::asset('js/jquery-2.1.1.js') }}"></script>
        <script src="{{ \Illuminate\Support\Facades\URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{ asset('js/inspinia.js') }}"></script>
        <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

        <!-- iCheck -->
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
        <script src="{{ \Illuminate\Support\Facades\URL::asset('js/plugins/toastr/toastr.min.js') }}"></script>
        @if(\Illuminate\Support\Facades\Session::has('message'))
            <script type="text/javascript">
                toastr.{{ \Illuminate\Support\Facades\Session::get('status') }}('{{ \Illuminate\Support\Facades\Session::get('message') }}', '{{ \Illuminate\Support\Facades\Session::get('title') }}');
            </script>
        @endif
    @yield('theJs')
    </body>
</html>
