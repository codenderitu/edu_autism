@extends('layouts.parentMain')
@section('theTitle') Parent | Update Password @endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('parentHome') }}">Home</a>
                </li>
                <li class="active">
                    <strong> <a href="{{ route('parentUpdatePassword') }}">Update Password</a> </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Update Password</h5>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" action="{{ route('postParentUpdatePassword') }}" method="post">
                            <p>Fill in all details correctly</p>
                            <div class="form-group"><label class="col-lg-2 control-label">Current Password</label>
                                <div class="col-lg-10">
                                    <input type="password" name="currpass" required placeholder="Current Password" class="form-control">
                                    <span class="help-block m-b-none">
                                        Enter your current password here
                                    </span>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">New Password</label>
                                <div class="col-lg-10">
                                    <input type="password" name="newpass" required placeholder="New Password" class="form-control">
                                    <span class="help-block m-b-none">
                                        Enter the new password here
                                    </span>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Confirm Password</label>
                                <div class="col-lg-10">
                                    <input type="password" name="conpass"  required placeholder="Confirm Password" class="form-control">
                                    <span class="help-block m-b-none">
                                        Confirm the password above
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                {{ csrf_field() }}
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-md btn-success btn-outline" type="submit">
                                        Update Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('theJs')
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
@endsection