@extends('layouts.parentMain')
@section('theTitle') Parent | Profile @endsection
@section('theHeadCss')

@endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('parentHome') }}">Home</a>
                </li>
                <li class="active">
                    <strong> <a href="{{ route('parentProfile') }}">Profile</a> </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profile Detail</h5>
                    </div>
                    <div>
                        <div class="ibox-content no-padding border-left-right">
                            <img alt="image" class="img-responsive" src="{{ (new \App\Http\Controllers\FuncController())->getProffpic() }}">
                        </div>
                        <div class="ibox-content profile-content">
                            <h4><strong>{{ \Illuminate\Support\Facades\Auth::user()->name }}</strong></h4>
                            <p><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse(\Illuminate\Support\Facades\Auth::user()->created_at)->diffForHumans() }} </p>
                            <h5>
                                About My Child
                            </h5>
                            <p>
                                {!! \Illuminate\Support\Facades\Auth::user()->aboutchild !!}
                            </p>
                            <br>
                            <br>
                            <br>
                            <div class="user-button">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button data-toggle="modal" href="#modal-form" type="button" class="btn-block btn btn-primary btn-sm btn-block"><i class="fa fa-envelope"></i> Update Profile</button>
                                        <div id="modal-form" class="modal fade" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12"><h3 class="m-t-none m-b">Update Profile</h3>
                                                                <p>About My Child</p>
                                                                <form role="form" action="{{ route('postProfileUpdateProfile') }}" method="post" enctype="multipart/form-data">
                                                                    <div class="form-group">
                                                                        <textarea name="aboutchild" rows="7" placeholder="Write your update here" class="form-control">{!! \Illuminate\Support\Facades\Auth::user()->aboutchild !!}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="file" name="proffpic" accept=".jpg, .png" title="This is optional" class="form-control">
                                                                    </div>

                                                                    <div>
                                                                        {{ csrf_field() }}
                                                                        <button class="btn btn-block btn-sm btn-primary pull-center m-t-n-xs" type="submit">
                                                                            <strong>
                                                                                Submit
                                                                            </strong>
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('theJs')
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
@endsection