@extends('layouts.parentMain')
@section('theTitle') Parent | Caregivers @endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('parentHome') }}">Home</a>
                </li>
                <li class="active">
                    <strong> <a href="{{ route('parentCaregivers') }}">Care Givers</a> </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">

                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Available Caregivers</h5>
                        <div class="ibox-tools">
                            Choose one as your caregiver
                        </div>
                    </div>
                    <div class="ibox-content">


                        <div class="project-list">

                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td class="project-people">
                                            <img alt="image" class="img-circle" src="{{ asset('') }}img/a7.jpg">
                                        </td>
                                        <td class="project-title">
                                            <a href="{{ route('parentDiscussions') }}">Gender Zumwo</a>
                                            <br/>
                                            <small>Joined 6 months ago</small>
                                        </td>
                                        <td class="project-completion">
                                            <small>300 parents</small>
                                        </td>
                                        <td class="project-people">
                                            <small><b>Skills: </b></small>
                                            <div class="">
                                                Communication, Motivation, Entertainment
                                            </div>
                                        </td>
                                        <td class="project-actions">
                                            <a href="#" class="btn btn-info btn-outline btn-sm"><i class="fa fa-folder"></i> Connect </a>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('theJs')
    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

    <script>
        $(document).ready(function(){

            $('#loading-example-btn').click(function () {
                btn = $(this);
                simpleLoad(btn, true)

                // Ajax example
//                $.ajax().always(function () {
//                    simpleLoad($(this), false)
//                });

                simpleLoad(btn, false)
            });
        });

        function simpleLoad(btn, state) {
            if (state) {
                btn.children().addClass('fa-spin');
                btn.contents().last().replaceWith(" Loading");
            } else {
                setTimeout(function () {
                    btn.children().removeClass('fa-spin');
                    btn.contents().last().replaceWith(" Refresh");
                }, 2000);
            }
        }
    </script>
@endsection