<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        {{--<img alt="image" class="img-circle" src="{{ asset('img/profile_small.jpg') }}" />--}}
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">Kelvin Nderitu</strong>
                             </span>
                            <span class="text-muted text-xs block">
                                Parent
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li>
                <a href="{{ route('parentHome') }}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">
                        Home
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('parentDiscussions') }}">
                    <i class="fa fa-comments"></i>
                    <span class="nav-label">
                        Chats
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('parentProfile') }}">
                    <i class="fa fa-user"></i>
                    <span class="nav-label">
                        Profile
                    </span>
                </a>
            </li>
            {{--<li>--}}
                {{--<a href="{{ route('parentCaregivers') }}">--}}
                    {{--<i class="fa fa-users"></i>--}}
                    {{--<span class="nav-label">--}}
                        {{--Care Givers--}}
                    {{--</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li>
                <a href="{{ route('parentUpdatePassword') }}">
                    <i class="fa fa-ellipsis-v"></i>
                    <span class="nav-label">
                       &nbsp; Update Password
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('logout') }}">
                    <i class="fa fa-sign-out"></i>
                    <span class="nav-label">
                        Logout
                    </span>
                </a>
            </li>




        </ul>

    </div>
</nav>