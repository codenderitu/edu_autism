@extends('layouts.authLayouts')
@section('title') Forgot Password @endsection
@section('theContent')
    <div>
        <h3 class="logo-name" style="color: #001f03;">
            <i class="fa fa-unlock-alt"></i>
        </h3>
    </div>
    <h3>Forgot Password</h3>
    <p>Answer the Security Question correctly to help reset your password</p>
    <form class="m-t" role="form" method="post" action="">
        <div class="form-group">
            <br>
            What is the name of your mother
        </div>
        <div class="form-group">
            <input type="text" name="password" minlength="8" class="form-control" placeholder="Answer" title="Answer to the Security Question" required="">
        </div>
        {{ csrf_field() }}
        <button class="btn btn-primary block full-width m-b">
            <i class="fa fa-unlock"></i>
            Unlock
        </button>
        <br>
        <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Back to Login</a>
    </form>
    <p class="m-t"> <small>&copy; Jovi {{ date("Y") }}</small> </p>
@endsection