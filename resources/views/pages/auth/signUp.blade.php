@extends('layouts.authLayouts')

@section('title') Register @endsection

@section('theCSS')

@endsection

@section('theContent')
    <div>
        <h3 class="logo-name" style="color: #001f03;">
            <i class="fa fa-info-circle"></i>
        </h3>
    </div>
    <h3>Autism Education</h3>
    <p>Education Mobile Application for caregivers of children with Autism</p>
    <p><b>Please Register Here</b></p>
    <form id="signUpForm" class="m-t" role="form" method="post" action="{{ route('postSignUp') }}">
        <div class="form-group">
            <input type="text" name="name" class="form-control" minlength="5" placeholder="Name" title="Name" required="">
        </div>
        <div class="form-group text-left i-checks">
            <div class="radio">
                <label>
                    <input type="radio" checked="" value="parent" id="optionsRadios1" name="usertype"> &nbsp; Parent
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="caregiver" id="optionsRadios2" name="usertype"> &nbsp;  Caregiver
                </label>
            </div>
        </div>
        <div class="form-group">
            <input type="text" id="name" name="username" minlength="1" class="form-control" placeholder="Username" title="Enter your username" required="">
        </div>
        <div class="form-group">
            <select class="form-control m-b" name="sq1" required>
                <option value="">Security Question 1</option>
                <option value="What is your Nickname">What is your Nickname</option>
                <option value="What is your talent">What is your talent</option>
                <option value="What is your favourite movie">What is your favourite movie</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" name="ans1" class="form-control" minlength="1" placeholder="Answer" title="Answer 1" required="">
        </div>
        <div class="form-group">
            <select class="form-control m-b" name="sq2" required>
                <option>Security Question 2</option>
                <option value="What is your best series">What is your best series</option>
                <option value="What is your best pet">What is your best pet</option>
                <option value="What is your best song">What is your best song</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" name="ans2" class="form-control" minlength="5" placeholder="Answer" title="Answer 2" required="">
        </div>
        <div class="form-group">
            <input type="password" name="password" minlength="8" class="form-control" placeholder="Password" required="">
        </div>
        <div class="form-group">
            <input type="password" name="conpass" minlength="8" class="form-control" placeholder="Confirm Password" required="">
        </div>

        <div class="form-group" id="termdom">
            <div class="checkbox i-checks">
                <label>
                    <input type="checkbox" id="termsCheck" name="termsCheck">
                    Agree the terms and policy
                </label>
            </div>
        </div>
        {{ csrf_field() }}
        <button class="btn btn-primary block full-width m-b">Register</button>
        <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Login</a>
    </form>
    <p class="m-t"> <small>&copy; Jovi {{ date("Y") }}</small> </p>
@endsection

@section('theJs')
    <script type="text/javascript">
        {{--$('#signUpForm').on('submit', function (e) {--}}
            {{--var subdomain = $('#subdomain').val();--}}
            {{--var email = $('#email').val();--}}
            {{--// submit the subdomain for checkups--}}
            {{--$.post("{{ route('checkSubDomain') }}", {--}}
                {{--subdomain : subdomain,--}}
                {{--email : email,--}}
                {{--_token : "{{ csrf_token() }}"--}}
            {{--}, function(data){--}}
                {{--console.log(data);--}}
                {{--if(!data[0]){--}}
                    {{--e.preventDefault();--}}
                    {{--toastr.error('\"'+subdomain+'\" is not available as a subdomain, \n please choose another one', 'Sorry');--}}
                    {{--var anim = "tada";--}}
                    {{--$('#subdom').focus();--}}
                    {{--animSubDom(anim);--}}
                    {{--setTimeout(removeAnimClasses, 1000, anim);--}}
                {{--}--}}

                {{--if(!data[1]){--}}
                    {{--e.preventDefault();--}}
                    {{--toastr.error('\"'+email+'\" is not an available email, \n Try choosing another', 'Sorry');--}}
                    {{--var anim = "tada";--}}
                    {{--animEmail(anim);--}}
                    {{--setTimeout(removeAnimClasses, 1000, anim);--}}
                {{--}--}}

                {{--if(data[1] && data[0]){--}}
                    {{--if($('#termsCheck').prop("checked")){--}}
                        {{--$('#signUpForm').unbind('submit').submit();--}}
                    {{--}else{--}}
                        {{--e.preventDefault();--}}
                        {{--toastr.info('You must agree to the Terms and Conditions to continue', 'Error');--}}
                        {{--var flip = "flip";--}}
                        {{--termdom(flip);--}}
                        {{--setTimeout(removeAnimClasses, 1000, flip);--}}
                    {{--}--}}
                {{--}--}}
            {{--}, "json");--}}
            {{--e.preventDefault();--}}
        {{--});--}}

//        function animSubDom(anim){
//            $('#subdom').addClass('animated');
//            $('#subdom').addClass(anim);
//        }
//
//        function animEmail(anim){
//            $('#email').addClass('animated');
//            $('#email').addClass(anim);
//        }
//
//        function termdom(anim){
//            $('#termdom').addClass('animated');
//            $('#termdom').addClass(anim);
//        }
//
//        function removeAnimClasses(anim){
//            $('#subdom').removeClass('animated');
//            $('#subdom').removeClass(anim);
//
//            $('#termdom').removeClass('animated');
//            $('#termdom').removeClass(anim);
//
//            $('#email').removeClass('animated');
//            $('#email').removeClass(anim);
//        }
    </script>
@endsection