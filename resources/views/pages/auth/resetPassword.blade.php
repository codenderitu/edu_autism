@extends('layouts.authLayouts')

@section('title') Login @endsection

@section('theContent')
    <div>
        <h3 class="logo-name" style="color: #001f03;">
            <i class="fa fa-unlock-alt"></i>
        </h3>
    </div>
    <h3>Reset Password</h3>
    <p>Reset your password below</p>
    <form class="m-t" role="form" method="post" action="">
        <div class="form-group">
            <input type="password" name="password" minlength="8" class="form-control" placeholder="Password" title="password" required="">
        </div>
        <div class="form-group">
            <input type="password" name="conpass" minlength="8" class="form-control" placeholder="Confirm Password" title="Confirm Password" required="">
        </div>
        {{ csrf_field() }}
        <button class="btn btn-primary block full-width m-b">
            Reset
        </button>
        <a href="{{ route('login') }}"><small>Back to Login</small></a>

    </form>
    <p class="m-t"> <small>&copy; Jovi {{ date("Y") }}</small> </p>
@endsection