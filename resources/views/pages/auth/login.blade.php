@extends('layouts.authLayouts')

@section('title') Login @endsection

@section('theContent')
    <div>
        <h3 class="logo-name" style="color: #001f03;">
            <i class="fa fa-unlock-alt"></i>
        </h3>
    </div>
    <h3>Autism Education</h3>
    <p>Education Mobile Application for caregivers of children with Autism</p>
    <form class="m-t" role="form" method="post" action="{{ route('postLogin') }}">
        <div class="form-group">
            <input type="text" name="username" minlength="5" class="form-control" placeholder="Username" title="Enter your Username" required="">
        </div>
        <div class="form-group">
            <input type="password" name="password" minlength="8" class="form-control" placeholder="Password" title="password" required="">
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary block full-width m-b">
            <i class="fa fa-unlock-alt"></i>
            Login
        </button>
        {{--<a href="{{ route('forgotPassword') }}"><small>Forgot password?</small></a>--}}
        <p class="text-muted text-center"><small>Do not have an account?</small></p>
        <a class="btn btn-sm btn-white btn-block" href="{{ route('signup') }}">Create an account</a>
    </form>
    <p class="m-t"> <small>&copy; Jovi {{ date("Y") }}</small> </p>
@endsection