@extends('layouts.careGiverMain')
@section('theTitle') CareGiver | Home @endsection
@section('theHeadCss')
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('caregiverHome') }}">Home</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <form action="{{ route('careGiverFilterHome') }}" method="post" class="form-inline">
                                <div class="form-group text-left i-checks">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" checked="" value="parent" id="optionsRadios1" name="postertype"> &nbsp; Parents
                                        </label>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="caregiver" id="optionsRadios2" name="postertype"> &nbsp;  Caregivers
                                        </label>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="radio">
                                        {{ csrf_field() }}
                                        <button class="btn btn-xs btn-info btn-outline" type="submit">
                                            <i class="fa fa-filter"></i> Filter
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </h5>
                        <div class="ibox-tools">
                            <a data-toggle="modal" class="btn btn-xs btn-success" href="#modal-form"> <i class="fa fa-plus"></i> New Update</a>
                            <div id="modal-form" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12"><h3 class="m-t-none m-b">New Post</h3>
                                                    <p>This update will be available to all users, both parents and caregivers</p>
                                                    <form role="form" method="post" action="{{ route('postUpdateCaregiver') }}">
                                                        <div class="form-group">
                                                            <textarea name="post" type="text" rows="7" placeholder="Write your update here" class="form-control"></textarea>
                                                        </div>
                                                        <div>
                                                            {{ csrf_field() }}
                                                            <button class="btn btn-sm btn-primary pull-center m-t-n-xs" type="submit">
                                                                <strong>
                                                                    Post Update
                                                                </strong>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                @foreach($updates as $update)
                                <div class="feed-element">
                                    <a href="#" class="pull-left">
                                        @if(\Illuminate\Support\Facades\Auth::user()->proffpic == "")
                                            <img alt="image" class="img-circle" src="{{ asset('img/img.jpg') }}">
                                        @endif
                                        {{--<img alt="image" class="img-circle" src="{{ asset('img/a2.jpg') }}">--}}
                                    </a>
                                    <div class="media-body ">
                                        <small class="pull-right">{{ \Carbon\Carbon::parse($update->created_at)->diffForHumans() }}</small>
                                        <strong>{{ \App\User::where('id', $update->poster)->first()->name }}</strong> - <small>{{ ucfirst(\App\User::where('id', $update->poster)->first()->usertype) }}</small> <br>
                                        <small class="text-muted">{{ \Carbon\Carbon::parse($update->created_at)->toDayDateTimeString() }}</small>
                                        <div class="well">
                                            {!! $update->post !!}
                                        </div>
                                        {{--<div class="pull-right">--}}
                                            {{--<a class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Message</a>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            {{ $updates->links() }}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('theHeadCss')

@endsection
@section('theJs')
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection