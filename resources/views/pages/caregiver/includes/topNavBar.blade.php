<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
    </div>
    {{--<ul class="nav navbar-top-links navbar-right">--}}
        {{--<li class="dropdown">--}}
            {{--<a class="dropdown-toggle count-info" data-toggle="dropdown" onclick='window.location.href = "{{ route('careGiverDiscussions') }}"' >--}}
                {{--<i class="fa fa-envelope"></i>--}}
                {{--<span id="unreadNum" class="label label-info">--}}
                    {{--{{ \App\Chat::where('status', 'unread')->where('receiver', \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier())->count() }}--}}
                {{--</span>--}}
            {{--</a>--}}
        {{--</li>--}}
    {{--</ul>--}}
</nav>