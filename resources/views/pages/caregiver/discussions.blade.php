@extends('layouts.careGiverMain')
@section('theTitle') CareGiver | Discussions @endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('parentHome') }}">Home</a>
                </li>
                <li class="active">
                    <strong> <a href="{{ route('careGiverDiscussions') }}">Discussions</a> </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox chat-view">

                    <div class="ibox-title">
                        <small class="pull-right text-muted">
                            Last message:  {{ \Carbon\Carbon::parse(\App\Chat::where('receiver', \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier())->orderBy('id', 'desc')->first()->created_at)->diffForHumans() }}
                        </small>
                        Chats
                    </div>


                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-9 ">
                                <div class="chat-message-form">
                                    <div class="form-group">
                                        <form id="chatBox" action="">
                                            <input id="chatBox" class="form-control" style="height: 50px;" name="chatBox" placeholder="Enter message text (Press enter to send)">
                                        </form>
                                    </div>
                                </div>
                                <div id="chatdisc" class="chat-discussion">

                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list">
                                        @foreach($chatUsers as $chatUser)
                                            <div id="chatUserObject" data-value="{{ $chatUser->id }}" class="chat-user">
                                                <img class="chat-avatar" src="{{ asset('storage/images/'.$chatUser->proffpic) }}" alt="" >
                                                <div class="chat-user-name">
                                                    <span>{{ $chatUser->name }}</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('theJs')
    <script type="text/javascript">
        $(document).ready(function () {
            var lastMessage = 0;
            var chatUser = 0;
            $('#chatUserObject').on('click', function () {
                $('#chatdisc').empty();
                chatUser = $(this).data("value");
                var chid = $(this).data("value");
                // fetch all chats between this guy and current user in desc order
                var url = "{{ route('getChatListFor') }}";
                var data = {
                    chid : chid,
                    myid : "{{ \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier() }}"
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#chatdisc').append(data);
                    },
                    dataType: "html"
                });
            });

            $('#chatBox').on('submit', function (e) {
                e.preventDefault();
                if(chatUser === 0){
                    alert("Please select a user first");
                }else{
                    var text = $('input[name=chatBox]').val();
                    if(text === ""){
                        alert("Type something");
                    }else{
                        var url = "{{ route('postChat') }}";
                        var data = {
                            chid : chatUser,
                            myid : "{{ \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier() }}",
                            message: text
                        };

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function (data) {
                                $('input[name=chatBox]').value("");
                            },
                            dataType: text
                        });
                    }
                }
            });

            window.setInterval(function(){
                if(chatUser !== 0){
                    var url = "{{ route('getChatListFor') }}";
                    var data = {
                        chid : chatUser,
                        myid : "{{ \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier() }}"
                    };

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            $('#chatdisc').empty().append(data);
                        },
                        dataType: "html"
                    });
                }
            }, 2000);
        });
    </script>
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
@endsection