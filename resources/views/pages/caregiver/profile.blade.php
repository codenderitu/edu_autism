@extends('layouts.careGiverMain')
@section('theTitle') CareGiver | Profile @endsection
@section('theHeadCss')

@endsection
@section('theBody')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Autism Education</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('caregiverHome') }}">Home</a>
                </li>
                <li class="active">
                    <strong> <a href="{{ route('careGiverProfile') }}">Profile</a> </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row animated fadeInRight">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Profile Detail</h5>
                    </div>
                    <div>
                        <div class="ibox-content no-padding border-left-right">
                            @if(\Illuminate\Support\Facades\Auth::user()->proffpic != null && \Illuminate\Support\Facades\Auth::user()->proffpic != "")
                            <img alt="image" class="img-responsive" src="{{ asset('storage/images/'.\Illuminate\Support\Facades\Auth::user()->proffpic) }}">
                            @else
                            <img alt="image" class="img-responsive" src="{{ asset('img/profile_big.jpg') }}">
                            @endif

                        </div>
                        <div class="ibox-content profile-content">
                            <h4><strong>{{ \Illuminate\Support\Facades\Auth::user()->name }}</strong></h4>
                            <p><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse(\Illuminate\Support\Facades\Auth::user()->created_at)->diffForHumans() }}</p>
                            <h5>
                                Skills:
                            </h5>
                            <table class="table table-hover">
                                <tbody>
                                    @foreach(\App\Skill::where('user', \Illuminate\Support\Facades\Auth::user()->getAuthIdentifier())->get() as $skill)
                                    <tr>
                                        <td class="project-people text-center">
                                            {{ $skill->skill }}
                                        </td>
                                        <td class="project-actions">
                                            <a href="{{ route('caregiverDeleteSkill', ['$skill' => $skill->id]) }}" class="btn btn-danger btn-outline btn-sm">
                                                <i class="fa fa-folder"></i>
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>


                            <div class="user-button">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button data-toggle="modal" href="#modal-form" type="button" class="btn-block btn btn-primary btn-sm btn-block"><i class="fa fa-envelope"></i> Add Skills </button>
                                        <div id="modal-form" class="modal fade" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12"><h3 class="m-t-none m-b">Add a Skill</h3>
                                                                <p>Enter Skill</p>
                                                                <form role="form" method="post" action="{{ route('postAddCaregiverSkill') }}">
                                                                    <div class="form-group">
                                                                        <input type="text" maxlength="250" minlength="2" name="skill" placeholder="Enter Skill" class="form-control">
                                                                    </div>
                                                                    {{ csrf_field() }}
                                                                    <div>
                                                                        <button class="btn btn-block btn-sm btn-primary pull-center m-t-n-xs" type="submit">
                                                                            <strong>
                                                                                Submit
                                                                            </strong>
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="user-button">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button data-toggle="modal" href="#profile-form" type="button" class="btn btn-success btn-sm">
                                            <i class="fa fa-edit"></i>
                                            Update Profile
                                        </button>
                                        <div id="profile-form" class="modal fade" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12"><h3 class="m-t-none m-b">Update your profile</h3>
                                                                <p>Update profile</p>
                                                                <form role="form" method="post" action="{{ route('careGiveProfileUpdate') }}" enctype="multipart/form-data">
                                                                    <div class="form-group">
                                                                        <input type="text" maxlength="250" minlength="2" name="name" placeholder="Update Name" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="file" accept="*.jpg, *.png" name="proffpic" placeholder="Upload Proffpic" title="Upload your profile picture" class="form-control">
                                                                    </div>
                                                                    {{ csrf_field() }}
                                                                    <div>
                                                                        <button class="btn btn-block btn-sm btn-primary pull-center m-t-n-xs" type="submit">
                                                                            <strong>
                                                                                Update
                                                                            </strong>
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('theJs')
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ asset('js/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/demo/peity-demo.js') }}"></script>
@endsection