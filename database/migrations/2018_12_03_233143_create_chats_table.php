<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration{
    public function up(){
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender');
            $table->string('receiver');
            $table->text('message');
            $table->text('status');//->default('unread');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('chats');
    }
}
