<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatesTable extends Migration{
    public function up(){
        Schema::create('updates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('poster');
            $table->string('postertype');
            $table->text('post');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('updates');
    }
}
