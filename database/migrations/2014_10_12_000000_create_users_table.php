<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration{
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('usertype');
            $table->string('username');
            $table->string('sq1');
            $table->string('ans1');
            $table->string('sq2');
            $table->string('ans2');
            $table->string('status')->nullable()->default("off");
            $table->string('terms')->nullable()->default("off");
            $table->text('aboutchild')->nullable();
            $table->text('proffpic')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('users');
    }
}
