<?php

namespace App\Http\Middleware;

use App\Http\Controllers\FuncController;
use Closure;
use Illuminate\Support\Facades\Auth;

class Parents{
    public function handle($request, Closure $next){
        $user = Auth::user();
        if($user->usertype != "parent"){
            $func = new FuncController();
            Auth::logout();
            return $func->toRouteWithMessage("login","Please login", "", "info");
        }
        return $next($request);
    }
}
