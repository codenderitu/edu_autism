<?php

namespace App\Http\Controllers;

use App\Skill;
use App\Update;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FuncController extends Controller{
    public function getProffpic(){
        $user = Auth::user();
        if($user->proffpic == null || $user->proffpic == ""){
            return asset('img/profile_big.jpg');
        }else{
            return asset("storage/images/".$user->proffpic);
        }
    }

    public function generateRandomString($length = 4) {
        $characters = '23456789ABCEFGHJKMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function postAddCaregiverSkill(Request $request){
        if(Auth::user()->usertype == "caregiver"){
            $skill = new Skill();
            $skill->user = Auth::user()->getAuthIdentifier();
            $skill->skill = $request['skill'];
            if($skill->save()){
                return $this->backWithMessage("Success", "Skill has been added", "success");
            }else{
                return $this->backWithUnknownError();
            }
        }else{
            return $this->backWithMessage("Sorry", "Access Denied", "error");
        }
    }

    public function postUpdateCaregiver(Request $request){
        $update = new Update();
        $update->poster = Auth::user()->getAuthIdentifier();
        $update->postertype = Auth::user()->usertype;
        $update->post = $request['post'];
        if($update->save()){
            return $this->backWithMessage("Success", "Update has been posted and approved", "success");
        }else{
            return $this->backWithUnknownError();
        }
    }

    public function login(Request $request){
        if(Auth::attempt(['username' => $request['username'], 'password' => $request['password']])){
            $user = User::where('username', $request['username'])->first();
            if($user->usertype == "parent"){
                return $this->toRouteWithMessage('parentHome', 'Welcome', '', 'success');
            }elseif($user->usertype == "caregiver"){
                return $this->toRouteWithMessage('caregiverHome', 'Welcome', '', 'success');
            }else{
                return $this->backWithUnknownError();
            }
        }else{
            return $this->backWithMessage("Sorry", "Login failed", "error");
        }
    }

    public function signUp(Request $request){
        if($request['termsCheck'] != 'on'){
            return $this->backWithMessage("Sorry", "You have to check the terms and conditions", "error");
        }elseif($request['conpass'] != $request['password']){
            return $this->backWithMessage("Sorry", "Your password and confirmation do not match", "error");
        }elseif(User::where('username', $request['username'])->count() > 0){
            return $this->backWithMessage("Sorry", "That username has been taken", "error");
        }else{
            $user = new User();
            $user->name = $request['name'];
            $user->usertype = $request['usertype'];
            $user->username = $request['username'];
            $user->sq1 = $request['sq1'];
            $user->ans1 = $request['ans1'];
            $user->sq2 = $request['sq2'];
            $user->ans2 = $request['ans2'];
            $user->password = bcrypt($request['password']);
            if($user->save()){
                return $this->toRouteWithMessage("login", "Success", "Sign Up was successful, you may now login", "success");
            }else{
                return $this->backWithUnknownError();
            }
        }
    }

    public function backWithMessage($title, $message, $status){
        return redirect()->back()->with([
            'title' => $title,
            'message' => $message,
            'status' => $status
        ]);
    }

    public function justBack(){
        return redirect()->back();
    }

    public function toRouteWithMessage($route, $title, $message, $status){
        return redirect()->route($route)->with([
            'title' => $title,
            'message' => $message,
            'status' => $status
        ]);
    }

    public function backWithUnknownError(){
        return redirect()->back()->with([
            'title' => 'Sorry!!',
            'message' => 'A Fatal Error Occurred, We are however working on it',
            'status' => 'error'
        ]);
    }

    public function toRoute($route){
        return redirect()->route($route);
    }
}
